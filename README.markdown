Functions for the [Fish shell](http://fishshell.com) – the essentials, including the prompt.


# Installation

~~~~.sh
cd ~/.config/fish/
git clone https://gitlab.com/hook-dot-files/fish-functions.git functions
~~~~


# Dependencies

In addition to [Fish][fish], the following functions have the following run-time dependencies:

- `cascade_kwin` depends on [KDE’s window manager KWin][kde]
- `liferay_compare` depends on [moreutils][]
- `pandoc-document` and `pandoc-slides` depend on [Pandoc][pandoc]
- `tree` depends on [tree][]
- `ttlog` depends on [GTimeLog][gtimelog]
- `vless` depends on [Vim][vim]

[fish]: https://fishshell.com/
[gtimelog]: https://mg.pov.lt/gtimelog/
[kde]: https://kde.org
[moreutils]: https://joeyh.name/code/moreutils/
[pandoc]: http://pandoc.org/
[tree]: http://mama.indstate.edu/users/ice/tree/
[vim]: http://www.vim.org/

