function pandoc-document --description 'Pretty PDF document from Markdown'
	pandoc --latex-engine=xelatex -V papersize:a4 $argv
end
