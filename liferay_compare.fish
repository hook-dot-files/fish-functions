function liferay_compare --description 'List all Java source code files do *not* hold Liferay’s copyright statement'

	#  List all Java files (currently lazy scanning)
	ls **.{java,class,jar,war,ear,js,jsp,dpj,xrb} | sort > /home/hook/tmp/all_files

	#  List all Java files that include Liferay’s copyright statement
	grep "Liferay, Inc" **.{java,class,jar,war,ear,js,jsp,dpj,xrb} | cut -f 1 -d : | sort > /home/hook/tmp/liferay_files

	#  Output diff between the two
	combine /home/hook/tmp/all_files not /home/hook/tmp/liferay_files > /home/hook/tmp/diff_files
	echo ""
	echo "Binary files missing Liferay copyright statements:"
	grep -s {'class$','jar$','war$','ear$'} /home/hook/tmp/diff_files
	echo ""
	echo "Source files missing Liferay copyright statements:"
	grep -s {'java$','js$','jsp$','dpj$','xrb$'} /home/hook/tmp/diff_files

	#  Statistics
	echo "---"
    wc -l /home/hook/tmp/all_files
    wc -l /home/hook/tmp/liferay_files

end
