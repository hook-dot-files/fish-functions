function vless --description 'Vim as a less-like pager'
	command vim -u /usr/share/vim/vim80/macros/less.vim $argv
end
