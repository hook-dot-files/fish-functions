function pandoc-slides --description 'Pretty PDF slides from Markdown'
	pandoc -t beamer --latex-engine=xelatex -V theme:metropolis $argv
end
