function cpb --description 'Quick way to create a backup file'
	cp -i $argv{,.BAK_(date --iso-8601)}
end
