function tree --description 'Tree with colours and (by default) just two levels deep'
	pwd
    command tree -C -L 2 $argv
end
